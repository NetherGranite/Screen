Color3 = {}

function Color3.new(r, g, b) local t = {}

	--preconditions--
	if r ~= nil and (r < 0 or r > 255) then error("Attempt to pass an R value of " .. r .. " to a Color3 constructor") end
	if g ~= nil and (g < 0 or g > 255) then error("Attempt to pass an G value of " .. g .. " to a Color3 constructor") end
	if b ~= nil and (b < 0 or b > 255) then error("Attempt to pass an B value of " .. b .. " to a Color3 constructor") end

	--attributes--
	t.R = r or 255
	t.G = g or 255
	t.B = b or 255

	--methods--
	function t:unpack() return self.R, self.G, self.B end
	function t:copy() return Color3.new(self.R, self.G, self.B) end

	--metas--
	function t.__tostring(t) return "("..t.R..", "..t.G..", "..t.B..")" end

return setmetatable(t,t) end

return Color3
