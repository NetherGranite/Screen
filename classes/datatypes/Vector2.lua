Vector2 = {}

function Vector2.new(x, y) local t = {}

	--attributes--
	t.X = x or 0
	t.Y = y or 0

	--methods--
	function t:unpack() return self.X, self.Y end
	function t:copy() return Vector2.new(self.X, self.Y) end

	--metas--
	function t.__tostring(t) return "("..t.X..", "..t.Y..")" end

	function t.__concat(t, appendage) return "("..t.X..", "..t.Y..")"..appendage end

	function t.__add(a, b)
		if type(a) == "table" and type(b) == "number" then
			return Vector2.new(a.X+b, a.Y+b)
		elseif type(a) == "table" and type(b) == "table" then
			return Vector2.new(a.X+b.X, a.Y+b.Y)
		elseif type(a) == "number" and type(b) == "table" then
			return Vector2.new(a+b.X, a+b.Y)
		else
			error("Attempt to divide " .. type(a) .. " by " .. type(b))
		end
	end

	function t.__sub(a, b)
		if type(a) == "table" and type(b) == "number" then
			return Vector2.new(a.X-b, a.Y-b)
		elseif type(a) == "table" and type(b) == "table" then
			return Vector2.new(a.X-b.X, a.Y-b.Y)
		elseif type(a) == "number" and type(b) == "table" then
			return Vector2.new(a-b.X, a-b.Y)
		else
			error("Attempt to divide " .. type(a) .. " by " .. type(b))
		end
	end

	function t.__mul(a, b)
		if type(a) == "table" and type(b) == "number" then
			return Vector2.new(a.X*b, a.Y*b)
		elseif type(a) == "table" and type(b) == "table" then
			return Vector2.new(a.X*b.X, a.Y*b.Y)
		elseif type(a) == "number" and type(b) == "table" then
			return Vector2.new(a*b.X, a*b.Y)
		else
			error("Attempt to divide " .. type(a) .. " by " .. type(b))
		end
	end

	function t.__div(a, b)
		if type(a) == "table" and type(b) == "number" then
			return Vector2.new(a.X/b, a.Y/b)
		elseif type(a) == "table" and type(b) == "table" then
			return Vector2.new(a.X/b.X, a.Y/b.Y)
		elseif type(a) == "number" and type(b) == "table" then
			return Vector2.new(a/b.X, a/b.Y)
		else
			error("Attempt to divide " .. type(a) .. " by " .. type(b))
		end
	end

	function t.__mod(a, b)
		if type(a) == "table" and type(b) == "number" then
			return Vector2.new(a.X%b, a.Y%b)
		elseif type(a) == "table" and type(b) == "table" then
			return Vector2.new(a.X%b.X, a.Y%b.Y)
		elseif type(a) == "number" and type(b) == "table" then
			return Vector2.new(a%b.X, a%b.Y)
		else
			error("Attempt to divide " .. type(a) .. " by " .. type(b))
		end
	end

return setmetatable(t,t) end

return Vector2
