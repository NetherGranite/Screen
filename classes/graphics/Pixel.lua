Pixel = {}

function Pixel.new() local t = {}

	--attributes--
	t.pos = Vector2.new()
	t.rep = "rectangle"
	t.color = Color3.new()
	t.zindex = 0

return setmetatable(t,t) end

return Pixel
