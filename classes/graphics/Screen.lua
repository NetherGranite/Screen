Screen = {}

function Screen.new(p, s, r) local t = {}

	--attributes--
	t.pos = p or Vector2.new()
	t.size = s or Vector2.new()
	t.res = r or Vector2.new()
	t.pixels = {}

	--methods--
	function t:reposPixels()
		local increment = self.size/self.res
		for x = 1, self.res.X do
			for y = 1, self.res.Y do
				local pixel = self.pixels[x][y]
				pixel.pos = Vector2.new( (x-1)*increment.X , (y-1)*increment.Y ) + self.pos
				pixel.size = increment
			end
		end
	end

	function t:getpixel(target)
		local offset = target / self.size --get proportional distance from origin
		local absindex = offset * self.res --get absolute index
		local xindex, yindex = math.floor(absindex.X) + 1, math.floor(absindex.Y) + 1 --round the absolute index down to whole numbers and compensate for zero-indexing
		if xindex > self.res.X then xindex = -1 end
		if yindex > self.res.Y then yindex = -1 end
		return xindex, yindex, screen.pixels[xindex][yindex], target, offset, absindex
	end

	function t:lovedraw()
		for x = 1, screen.res.X do
			for y = 1, screen.res.Y do
				local pixel = screen.pixels[x][y]
				love.graphics.setColor(pixel.color.R, pixel.color.G, pixel.color.B)
				love.graphics.rectangle('fill', pixel.pos.X, pixel.pos.Y, pixel.size.X, pixel.size.Y)
			end
		end
	end

	--metas--
	function t.__call(t)
		t:lovedraw()
	end

	--init--
	for x = 1, t.res.X do
		t.pixels[x] = {}
		for y = 1, t.res.Y do
			t.pixels[x][y] = Pixel.new()
		end
	end

	t:reposPixels()

return setmetatable(t,t) end

return Screen
