--classes--
Vector2 = dofile([[classes\datatypes\Vector2.lua]])
Color3 = dofile([[classes\datatypes\Color3.lua]])
Pixel = dofile([[classes\graphics\Pixel.lua]])
Screen = dofile([[classes\graphics\Screen.lua]])

--options--
selectionstats = true

--LOVE--
function love.load()

	screen = Screen.new(Vector2.new(10,10),Vector2.new(400,400),Vector2.new(50,50))

end

function love.update(dt)

	xindex, yindex, pixel, target, offset, absindex = screen:getpixel(Vector2.new(love.mouse.getPosition()))
	if xindex >= 1 and yindex >=1 then
		pixel.color = Color3.new(255, 0, 0)
	end

end

function love.draw()

	screen()
	if selectionstats == true then love.graphics.setColor(255, 255, 255) love.graphics.print("Mouse: "..target.."\nOffset: "..offset.."\nAbsIndex: "..absindex.."\nPixel: ".."["..xindex.."]["..yindex.."]", 10, 500) end

end
